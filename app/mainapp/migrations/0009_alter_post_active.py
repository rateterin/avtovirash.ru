# Generated by Django 4.1.1 on 2023-01-14 15:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("mainapp", "0008_alter_category_alias_alter_post_alias"),
    ]

    operations = [
        migrations.AlterField(
            model_name="post",
            name="active",
            field=models.BooleanField(
                db_index=True, default=True, verbose_name="активна"
            ),
        ),
    ]
