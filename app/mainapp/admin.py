from django import forms
from django.contrib import admin
from django.db.models import Manager as DefaultManager

from django_ckeditor_5.widgets import CKEditor5Widget

from .models import Category, Post, Comment, Status, PostLikes, CommentLikes

from config import settings


class PostAdminForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditor5Widget())

    class Meta:
        model = Post
        fields = "__all__"


@admin.register(Category)
class CategoryModelAdmin(admin.ModelAdmin):
    list_display = ("id", "name", "active", "order",)
    list_editable = ("active", "order",)
    # prepopulated_fields = {"alias": ("name",)}


@admin.register(Post)
class PostModelAdmin(admin.ModelAdmin):
    form = PostAdminForm
    list_display = ("id", "title", "category", "user_id", "active", "order",)
    list_editable = ("active", "order",)
    list_display_links = (
        "id",
        "title",
    )
    ordering = (
        "active",
        "-created_at",
        "-updated_at",
    )
    list_filter = (
        "category",
        "active",
        "is_deleted",
    )
    # prepopulated_fields = {"alias": ("title",)}

    def get_queryset(self, request):
        return self.model.objects_all.all()


if settings.COMMENTS_ENABLED:
    @admin.register(Comment)
    class CommentModelAdmin(admin.ModelAdmin):
        list_display = (
            "id",
            "active",
            "text",
            "post_id",
            "user_id",
            "created_at",
            "updated_at",
        )
        list_editable = ("active",)
        list_display_links = (
            "id",
            "post_id",
            "user_id",
        )
        ordering = (
            "active",
            "-created_at",
            "-updated_at",
        )
        list_filter = (
            "active",
            "is_deleted",
        )

        def get_queryset(self, request):
            return self.model.objects_all.all()


# @admin.register(Status)
# class StatusModelAdmin(admin.ModelAdmin):
#     list_display = ["id", "name"]


if settings.LIKES_ENABLED:
    @admin.register(PostLikes)
    class PostLikesModelAdmin(admin.ModelAdmin):
        list_display = ["id", "post_id", "user_id", "status", "active"]


if settings.COMMENTS_ENABLED and settings.LIKES_ENABLED:
    @admin.register(CommentLikes)
    class CommentLikesModelAdmin(admin.ModelAdmin):
        list_display = ["id", "comment_id", "user_id", "status", "active"]
