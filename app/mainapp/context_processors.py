from mainapp.models import Category, Post

from config import settings


def menu(request):
    show_active = not (request.user.is_superuser)
    menu = Category.objects.all()
    if show_active:
        menu = menu.filter(active=True)
    menu = list(menu.order_by("order").values())
    for item in menu:
        posts = Post.objects.filter(category=item["id"])
        if show_active:
            posts = posts.filter(active=True)
        item.update(posts=list(posts.order_by("order").values()))

    return {"menu": menu}


def enabled_functional(request):
    context = {
        "search_enabled": settings.SEARCH_ENABLED,
        "comments_enabled": settings.COMMENTS_ENABLED,
        "likes_enabled": settings.LIKES_ENABLED,
        "api_enabled": settings.API_ENABLED,
        "posts_count_enabled": settings.POSTS_COUNT,
        "posts_sorting_enabled": settings.POSTS_SORTING,
        "posts_pagination_enabled": settings.POSTS_PAGINATION,
    }

    return context


def branding(request):
    context = {
        "title": settings.TITLE,
        "copyright": settings.COPYRIGHT,
    }

    return context


def demo_categories(request):
    demo_categories = ("Дизайн", "Веб-разработка", "Мобильная разработка", "Маркетинг")
    categories = Category.objects.values_list("name", flat=True)
    result = all(dc in categories for dc in demo_categories)
    return {"demo_categories": not result}
