from django.forms import ModelForm
from django import forms
from django_ckeditor_5.widgets import CKEditor5Widget

from .models import Post, Category, Comment

from config import settings


class PostForm(ModelForm):
    category = forms.ModelChoiceField(
        queryset=Category.objects.all(),
    )
    category.widget.attrs.update({"class": "form-control"})

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["category"].label = "Раздел"
        self.fields["content"].label = "Содержимое"

    class Meta:
        model = Post
        widgets = {
            "title": forms.TextInput(attrs={"class": "form-control"}),
            "order": forms.NumberInput(attrs={"class": "form-control"}),
            "content": CKEditor5Widget(attrs={"class": "django_ckeditor_5"}),
        }
        fields = ("title", "category", "order", "content")
        exclude = [
            "user",
            "active",
            "description",
            "image",
            "is_deleted",
            "created_at",
            "updated_at",
            "objects",
            "alias",
        ]


if settings.COMMENTS_ENABLED:
    class CommentForm(forms.ModelForm):
        class Meta:
            model = Comment
            fields = ("text",)
            widgets = {
                "text": forms.Textarea(attrs={"class": "form-control"}),
            }
else:
    class CommentForm:
        pass
