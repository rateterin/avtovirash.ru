from django.db import models
from transliterate import slugify

from django_ckeditor_5.fields import CKEditor5Field
from django.urls import reverse

from autoslug import AutoSlugField

from config import settings


class NotDeletedManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(is_deleted=False)


class Category(models.Model):
    name = models.CharField(max_length=45, verbose_name="наименование", unique=True)
    description = models.CharField(max_length=300, blank=True, verbose_name="описание")
    alias = AutoSlugField(populate_from=lambda instance: slugify(instance.name, "ru"), always_update=True, allow_unicode=True, unique=True, null=True, verbose_name="Имя для адресной строки")
    active = models.BooleanField(default=True, verbose_name="активна")
    order = models.IntegerField(default=0, verbose_name="Порядок")
    objects = models.Manager()

    def __str__(self):
        return f'{self.name}{"" if self.active else "(блок)"}'

    class Meta:
        verbose_name = "раздел"
        verbose_name_plural = "разделы"


class Status(models.Model):
    name = models.CharField(
        max_length=45, verbose_name="наименование статуса", unique=True
    )

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = "Статус страницы"
        verbose_name_plural = "Статусы страниц"


class Post(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name="Автор", on_delete=models.CASCADE
    )
    title = models.CharField(
        verbose_name="Заголовок страницы", max_length=110, unique=True
    )
    alias = AutoSlugField(populate_from=lambda instance: slugify(instance.title, "ru"), always_update=True, allow_unicode=True, unique=True, null=True, verbose_name="Имя для адресной строки")
    description = models.TextField(max_length=300, blank=True, verbose_name="описание")
    category = models.ForeignKey(
        Category,
        verbose_name="Раздел страницы",
        on_delete=models.CASCADE,
        related_name="posts",
        null=False,
    )
    active = models.BooleanField(verbose_name="активна", default=True, db_index=True)
    is_deleted = models.BooleanField(
        verbose_name="Удалена", default=False, db_index=True
    )
    # status = models.ForeignKey(Status, verbose_name='Статусы статьи', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    image = models.ImageField(
        null=True,
        verbose_name="Главное изображение статьи",
        upload_to="post_image",
        blank=True,
        max_length=150,
    )
    content = CKEditor5Field(null=True, blank=True)
    order = models.IntegerField(default=0, verbose_name="Порядок")
    objects_all = models.Manager()
    objects = NotDeletedManager()

    def __str__(self):
        return f'{self.title}{"" if self.active else "(блок)"}'

    @property
    def likes_count(self):
        return self.post_likes.filter(status=True, active=True).count()

    class Meta:
        verbose_name = "страница"
        verbose_name_plural = "страницы"


if settings.COMMENTS_ENABLED:
    class Comment(models.Model):
        user = models.ForeignKey(
            settings.AUTH_USER_MODEL, verbose_name="Автор", on_delete=models.CASCADE
        )
        parent = models.ForeignKey(
            "self",
            verbose_name="Родитель",
            on_delete=models.SET_NULL,
            blank=True,
            null=True,
        )
        post = models.ForeignKey(
            Post,
            related_name="comments",
            verbose_name="Название статьи",
            on_delete=models.CASCADE,
        )
        text = models.TextField(verbose_name="Комментарий")
        active = models.BooleanField(verbose_name="активна", default=False, db_index=True)
        is_deleted = models.BooleanField(
            verbose_name="Удалена", default=False, db_index=True
        )
        created_at = models.DateTimeField(auto_now_add=True)
        updated_at = models.DateTimeField(auto_now=True)
        objects_all = models.Manager()
        objects = NotDeletedManager()

        def __str__(self):
            return f"{self.post}"

        def delete(self):
            self.is_deleted = True
            self.save()

        @property
        def likes_count(self):
            return self.comment_likes.filter(status=True, active=True).count()

        class Meta:
            verbose_name = "коментарий"
            verbose_name_plural = "коментарии"
else:
    class Comment:
        pass


if settings.LIKES_ENABLED and settings.COMMENTS_ENABLED:
    class CommentLikes(models.Model):
        comment = models.ForeignKey(
            Comment,
            verbose_name="Название статьи",
            related_name="comment_likes",
            on_delete=models.CASCADE,
        )
        status = models.BooleanField(default=True, verbose_name="Статус")
        user = models.ForeignKey(
            settings.AUTH_USER_MODEL,
            verbose_name="Автор",
            related_name="comment_likes",
            on_delete=models.CASCADE,
        )
        active = models.BooleanField(verbose_name="активна", default=True, db_index=True)

        class Meta:
            verbose_name = "Лайк к коментарию"
            verbose_name_plural = "Лайки к коментариям"
            unique_together = ("comment_id", "user_id")
else:
    class CommentLikes:
        pass


if settings.LIKES_ENABLED:
    class PostLikes(models.Model):
        post = models.ForeignKey(
            Post,
            verbose_name="Название статьи",
            related_name="post_likes",
            on_delete=models.CASCADE,
        )
        status = models.BooleanField(default=True, verbose_name="Статус")
        user = models.ForeignKey(
            django_settings.AUTH_USER_MODEL,
            verbose_name="Автор",
            related_name="post_likes",
            on_delete=models.CASCADE,
        )
        active = models.BooleanField(verbose_name="активна", default=True, db_index=True)

        class Meta:
            verbose_name = "Лайк к статье"
            verbose_name_plural = "Лайки к статьям"
            unique_together = ("post_id", "user_id")
else:
    class PostLikes:
        pass
